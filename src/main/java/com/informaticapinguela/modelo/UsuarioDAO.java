package com.informaticapinguela.modelo;

import java.io.Reader;
import java.util.ArrayList;
import java.util.List;

import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.persistence.StoredProcedureQuery;
import javax.persistence.TypedQuery;
import javax.ws.rs.client.Entity;
import javax.ws.rs.core.GenericType;
import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.Response;

import org.jboss.resteasy.client.jaxrs.ResteasyClient;
import org.jboss.resteasy.client.jaxrs.ResteasyClientBuilder;
import org.jboss.resteasy.client.jaxrs.ResteasyWebTarget;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.informaticapinguela.backing.Credenciais;

/**
 * Clase para realizar consultas sobre los usuarios a la base de datos
 * @author Rubén Martínez Quiroga
 * @version 1.5
 */
@Stateless
public class UsuarioDAO {
	private final String URL_BASE="http://localhost:8081/api/rest";
	@PersistenceContext
	private EntityManager em;
	
	@Inject
	private Credenciais credenciais;
	
	/**
	 * Método para listar todos los usuarios
	 * @return usuarios
	 */

	public ArrayList<Usuario> getUsuarios() {
		String URL_PUNTO_SERVICIO = "/usuarios";
		ResteasyClient client = new ResteasyClientBuilder().build();
		ResteasyWebTarget target = client.target(URL_BASE+URL_PUNTO_SERVICIO);
		ArrayList<Usuario> usuarios = 
		     target.request().header("Authorization", "Bearer "+credenciais.getToken()).get(new GenericType<ArrayList<Usuario>>() {});
		client.close();
		return usuarios;
	}
	/**
	 * Método para añadir un usuario con rol por defecto user
	 * @param u nuevo
	 */
	public void addUsuario(Usuario u) {
		String URL_PUNTO_SERVICIO = "/usuarios";
		ResteasyClient client = new ResteasyClientBuilder().build();
		ResteasyWebTarget target = client.target(URL_BASE+URL_PUNTO_SERVICIO);
		
		List<Rol> roles= new ArrayList<Rol>();
		roles.add(Rol.ROLE_USER);
		u.setRoles(roles);
		Response response = target.request().post(Entity.entity(u, "application/json"));
		System.out.println("addUsuario: "+response.getStatus());
		response.close();
	}
	
	
	public void uploadImage(Usuario u) {
		String URL_PUNTO_SERVICIO = u.getId()+"/usuarios/upload";
		ResteasyClient client = new ResteasyClientBuilder().build();
		ResteasyWebTarget target = client.target(URL_BASE+URL_PUNTO_SERVICIO);
		Response response = target.request().header("Authorization", "Bearer "+credenciais.getToken()).post(Entity.entity(u, "application/json"));
		response.close();
	}
	
	/**
	 * Método para validar un usuario por email y contrasena
	 * @param email nuevo
	 * @param contrasena nuevo
	 * @return usuario
	 */
	public Usuario validaUsuario(String email, String contrasena) {
		String URL_PUNTO_SERVICIO = "/api/auth";

		ResteasyClient client = new ResteasyClientBuilder().build();
		Usuario usuario = new Usuario();
		usuario.setEmail(email);
		usuario.setContrasena(contrasena);
		
		ResteasyWebTarget target = client.target(URL_BASE+URL_PUNTO_SERVICIO);
		Response responseToken = target.request().post(Entity.entity(usuario, "application/json"));

		int status=responseToken.getStatus();
		System.out.println("Status: "+status);
		Usuario usuarioResposta=null;
		if(status==200) {	

			String authorizationHeader = responseToken.getHeaderString(HttpHeaders.AUTHORIZATION);	
			responseToken.close();
			System.out.println("UsuadioDAO - JSF: Token : "+ authorizationHeader );
			String token = authorizationHeader.substring(authorizationHeader.indexOf("Bearer")+6).trim();
			System.out.println("UsuarioDAO TOKEN: "+token);

			credenciais.setToken(token);
			
			ResteasyWebTarget target2 = client.target(URL_BASE+"/usuarios/token/"+token);
			Response responseUsuario = target2.request().header("Authorization", authorizationHeader).get();
			usuarioResposta = responseUsuario.readEntity(Usuario.class);
			System.out.println("UsuarioDAO - JSF: El usuario logueado es: "+ usuarioResposta);
			responseUsuario.close();
			return usuarioResposta;
					
		}else if(status==401) {
			System.out.println("Error 401");
		}
		return usuarioResposta;
	}
}
