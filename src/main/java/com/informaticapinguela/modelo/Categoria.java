package com.informaticapinguela.modelo;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.inject.Named;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedStoredProcedureQuery;
import javax.persistence.OneToMany;

/**
 * Clase para obtener la información de las categorías
 * @author Rubén Martínez Quiroga
 * @version 1.5
 */
@Named
public class Categoria implements Serializable{
	private static final long serialVersionUID = 1L;
	//ATRIBUTOS
	private long id;
	protected  String nombre;
	//GET Y SET
	/**
	 * Devuelve el nombre guardado
	 * @return nombre guardado
	 */
	public String getNombre() {
		return nombre;
	}
	
	public Categoria(long id) {
		super();
		this.id = id;
	}
	
	public Categoria() {
		
	}
	/**
	 * Configura un nuevo nombre
	 * @param nombreCategoria nuevo
	 */
	public void setNombre(String nombreCategoria) {
		this.nombre = nombreCategoria;
	}

	/**
	 * Devuelve los datos de la clase
	 */
	public String toString() {
		return nombre;
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((nombre == null) ? 0 : nombre.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Categoria other = (Categoria) obj;
		if (nombre == null) {
			if (other.nombre != null)
				return false;
		} else if (!nombre.equals(other.nombre))
			return false;
		return true;
	}
	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
}
