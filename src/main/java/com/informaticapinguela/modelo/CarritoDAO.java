package com.informaticapinguela.modelo;

import java.io.Serializable;
import java.util.ArrayList;

import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.ws.rs.client.Entity;
import javax.ws.rs.core.GenericType;
import javax.ws.rs.core.Response;

import org.jboss.resteasy.client.jaxrs.ResteasyClient;
import org.jboss.resteasy.client.jaxrs.ResteasyClientBuilder;
import org.jboss.resteasy.client.jaxrs.ResteasyWebTarget;

import com.informaticapinguela.backing.Credenciais;

@Stateless
public class CarritoDAO implements Serializable{
	private static final long serialVersionUID = 1L;
	private final String URL_BASE="http://localhost:8081/api/rest";

	
	@PersistenceContext
	private EntityManager em;
	
	@Inject
	private Credenciais credenciais;
	
	public ArrayList<Carrito> getCarrito() {
		String URL_PUNTO_SERVICIO = "/carritos";
		ResteasyClient client = new ResteasyClientBuilder().build();
		ResteasyWebTarget target = client.target(URL_BASE+URL_PUNTO_SERVICIO);
		ArrayList<Carrito> carritos = 
				target.request().header("Authorization", "Bearer "+credenciais.getToken()).get(new GenericType<ArrayList<Carrito>>() {});
		return carritos;
	}
	
	public Carrito addCarrito(Carrito c) {
		String URL_PUNTO_SERVICIO = "/carritos/usuario/"+c.getUsuario().getId();
		ResteasyClient client = new ResteasyClientBuilder().build();
		ResteasyWebTarget target = client.target(URL_BASE+URL_PUNTO_SERVICIO);
		Response response = target.request().header("Authorization", "Bearer "+credenciais.getToken()).post(Entity.entity(c, "application/json"));
		Carrito carrito=response.readEntity(Carrito.class);
		System.out.println("CarritoDAO, addCarrito: "+response.getStatus());
		response.close();
		return carrito;
	}
	
	public ArrayList<Carrito> getCarritoPorId(Carrito c) {
		String URL_PUNTO_SERVICIO = "/carritos/"+c.getId();
		ResteasyClient client = new ResteasyClientBuilder().build();
		ResteasyWebTarget target = client.target(URL_BASE+URL_PUNTO_SERVICIO);
		ArrayList<Carrito> carrito = 
				target.request().header("Authorization", "Bearer "+credenciais.getToken()).get(new GenericType<ArrayList<Carrito>>() {});
		return carrito;
	}
	
	
	public void asignarItemACarrito(Carrito c, Item i) {
		String URL_PUNTO_SERVICIO = "/items/"+i.getId()+"/carrito/"+c.getId();
		ResteasyClient client = new ResteasyClientBuilder().build();
		ResteasyWebTarget target = client.target(URL_BASE+URL_PUNTO_SERVICIO);
		Response response = target.request().header("Authorization", "Bearer "+credenciais.getToken()).put(Entity.entity(i, "application/json"));
		response.close();
	}
	
	public void borrar(Carrito c) {
		String URL_PUNTO_SERVICIO = "/carritos/"+c.getId();
		ResteasyClient client = new ResteasyClientBuilder().build();
		ResteasyWebTarget target = client.target(URL_BASE+URL_PUNTO_SERVICIO);
		Response response = target.request().header("Authorization", "Bearer "+credenciais.getToken()).delete();	
		response.close();
	}
	
	
	
}
