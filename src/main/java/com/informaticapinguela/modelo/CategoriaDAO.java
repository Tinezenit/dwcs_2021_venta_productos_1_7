package com.informaticapinguela.modelo;

import java.io.Serializable;
import java.util.ArrayList;

import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.ws.rs.core.GenericType;

import org.jboss.resteasy.client.jaxrs.ResteasyClient;
import org.jboss.resteasy.client.jaxrs.ResteasyClientBuilder;
import org.jboss.resteasy.client.jaxrs.ResteasyWebTarget;

import com.informaticapinguela.backing.Credenciais;

@Stateless
public class CategoriaDAO implements Serializable{
	private static final long serialVersionUID = 1L;
	private final String URL_BASE="http://localhost:8081/api/rest";
	
	@PersistenceContext
	private EntityManager em;
	
	@Inject
	private Credenciais credenciais;
	
	/**
	 * Método para listar todas las categorias
	 * @return categorias
	 */
	public ArrayList<Categoria> getCategorias() {
		String URL_PUNTO_SERVICIO = "/categorias";
		ResteasyClient client = new ResteasyClientBuilder().build();
		ResteasyWebTarget target = client.target(URL_BASE+URL_PUNTO_SERVICIO);
		ArrayList<Categoria> categorias = 
				target.request().header("Authorization", "Bearer "+credenciais.getToken()).get(new GenericType<ArrayList<Categoria>>() {});
		return categorias;
	}
	
	public ArrayList<Categoria> getCategoriaPorId(Categoria c) {

		String URL_PUNTO_SERVICIO = "/categorias/"+c.getId();
		ResteasyClient client = new ResteasyClientBuilder().build();
		ResteasyWebTarget target = client.target(URL_BASE+URL_PUNTO_SERVICIO);
		ArrayList<Categoria> categorias = 
				target.request().header("Authorization", "Bearer "+credenciais.getToken()).get(new GenericType<ArrayList<Categoria>>() {});
		return categorias;
	}
}
