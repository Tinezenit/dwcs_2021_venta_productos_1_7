package com.informaticapinguela.modelo;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

/**
 * Clase para cargar datos a la base de datos
 * @author Rubén Martínez Quiroga
 * @version 1.5
 */
@Stateless
public class ConsultaDAO {
	@PersistenceContext
	private EntityManager entityManager;
	/**
	 * Método para crear datos hardcore
	 */
	public void cargarDatos() {
		/*
		Categoria cat1= new Categoria();
		cat1.setNombre("peliculas");
		
		Producto p1= new Producto();
		p1.setNombre("Superman");
		p1.setPrecio(20.90);
		p1.setStock(10);
		
		Producto p2= new Producto();
		p2.setNombre("Spiderman");
		p2.setPrecio(40.90);
		p2.setStock(20);
		
		cat1.addProducto(p1);
		cat1.addProducto(p2);
		
		entityManager.persist(cat1);
		*/
		/*
		List<Rol> roles= new ArrayList<Rol>();
		Rol r1 = null;
		roles.add(r1.ROLE_ADMIN);
		Usuario u1= new Usuario();
		u1.setNombre("admin");
		u1.setContrasena("abc123.");
		u1.setEmail("admin@hotmail.com");
		u1.setDireccion("C/Roberto Baamonde");
		u1.setTelefono("663847878");
		u1.setRoles(roles);
		
		entityManager.persist(u1);*/
//		StoredProcedureQuery query = entityManager.createNamedStoredProcedureQuery("MOSTRAR_USUARIOS");
//        
//		List<Usuario> result = query.getResultList();
//		for(int i=0; i<result.size(); i++) {
//			Usuario usuario = (Usuario)result.get(i);
//		    System.out.println(usuario.getNombre());
//		}
		/*
		List<Rol> roles= new ArrayList<Rol>();
		Rol r2 = null;
		roles.add(r2.ROLE_USER);
		Usuario u2= new Usuario();
		u2.setNombre("user");
		u2.setContrasena("abc123.");
		u2.setEmail("user@hotmail.com");
		u2.setDireccion("C/Doctor Casares");
		u2.setTelefono("635485968");
		u2.setRoles(roles);
		
		entityManager.persist(u2);
		*/
	} 
}
