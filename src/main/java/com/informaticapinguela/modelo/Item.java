package com.informaticapinguela.modelo;

import java.io.Serializable;

import javax.enterprise.context.Dependent;
import javax.inject.Named;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;

/**
 * Clase para obtener la información de los items
 * @author Rubén Martínez Quiroga
 * @version 1.5
 */
@Named
@Entity
@Dependent
public class Item implements Serializable{
	private static final long serialVersionUID = 1L;
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private long id;
	private int numUnidades;
	private Producto producto;
	private Carrito carrito;
	
	
	/**
	 * Obtener la id
	 * @return id
	 */
	public long getId() {
		return id;
	}
	/**
	 * Crear una id
	 * @param id nuevo
	 */
	public void setId(long id) {
		this.id = id;
	}
	/**
	 * Obtener el numero de unidades
	 * @return numUnidades 
	 */
	public int getNumUnidades() {
		return numUnidades;
	}
	/**
	 * Crear un numero de unidades
	 * @param numUnidades nuevo
	 */
	public void setNumUnidades(int numUnidades) {
		this.numUnidades = numUnidades;
	}
	/**
	 * Obtener un producto
	 * @return producto
	 */
	public Producto getProducto() {
		return producto;
	}
	/**
	 * Configurar un producto
	 * @param producto nuevo
	 */
	public void setProducto(Producto producto) {
		this.producto = producto;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + (int) (id ^ (id >>> 32));
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Item other = (Item) obj;
		if (id != other.id)
			return false;
		return true;
	}

	public Carrito getCarrito() {
		return carrito;
	}
	public void setCarrito(Carrito carrito) {
		this.carrito = carrito;
	}
	@Override
	public String toString() {
		return "Item [id=" + id + ", numUnidades=" + numUnidades + ", producto=" + producto + ", carrito=" + carrito
				+ "]";
	}
	
}

