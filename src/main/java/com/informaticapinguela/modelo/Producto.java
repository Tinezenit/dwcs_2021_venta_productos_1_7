package com.informaticapinguela.modelo;

import java.io.Serializable;
import javax.enterprise.context.Dependent;
import javax.inject.Named;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.NamedStoredProcedureQueries;
import javax.persistence.NamedStoredProcedureQuery;


/**
 * Clase para obtener la información de los productos
 * @author Rubén Martínez Quiroga
 * @version 1.5
 */
@Named
@Dependent
public class Producto implements Serializable{
	private static final long serialVersionUID = 1L;
	private long id;
	private String nombre;
	private double precio;
	private int stock;
	private String image;
	private int rating;
	@Enumerated(EnumType.STRING)
	private InventoryStatus inventoryStatus;
	
	private Categoria categoria;
	
	public Producto(Categoria categoria) {
		super();
		this.categoria = categoria;
	}
	
	public Producto() {
		super();
	}

	/**
	 * Obtener una id
	 * @return id
	 */
	public long getId() {
		return id;
	}
	/**
	 * Configurar una id
	 * @param id nuevo
	 */
	public void setId(long id) {
		this.id = id;
	}
	/**
	 * Obtener un nombre
	 * @return nombre
	 */
	public String getNombre() {
		return nombre;
	}
	/**
	 * Configurar un nombre
	 * @param nombre nuevo
	 */
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	/**
	 * Obtener un precio
	 * @return precio
	 */
	public double getPrecio() {
		return precio;
	}
	/**
	 * Configurar un precio
	 * @param precio nuevo
	 */
	public void setPrecio(double precio) {
		this.precio = precio;
	}
	/**
	 * Obtener un stock
	 * @return stock
	 */
	public int getStock() {
		return stock;
	}
	/**
	 * Configurar un stock
	 * @param stock nuevo
	 */
	public void setStock(int stock) {
		this.stock = stock;
	}
	/**
	 * Obtener una categoria
	 * @return categoria
	 */
	public Categoria getCategoria() {
		return categoria;
	}
	/**
	 * Configurar una categoria
	 * @param categoria nueva
	 */
	public void setCategoria(Categoria categoria) {
		this.categoria = categoria;
	}
	@Override
	public String toString() {
		return "Producto [id=" + id + ", nombre=" + nombre + ", precio=" + precio + ", stock=" + stock + ", categoria="
				+ "]";
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + (int) (id ^ (id >>> 32));
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Producto other = (Producto) obj;
		if (id != other.id)
			return false;
		return true;
	}

	public String getImage() {
		return image;
	}

	public void setImage(String image) {
		this.image = image;
	}

	public InventoryStatus getInventoryStatus() {
		return inventoryStatus;
	}

	public void setInventoryStatus(InventoryStatus inventoryStatus) {
		this.inventoryStatus = inventoryStatus;
	}

	public int getRating() {
		return rating;
	}

	public void setRating(int rating) {
		this.rating = rating;
	}
	

}
