package com.informaticapinguela.modelo;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.Serializable;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;

import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.StoredProcedureQuery;
import javax.ws.rs.client.Entity;
import javax.ws.rs.core.GenericType;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.jboss.resteasy.client.jaxrs.ResteasyClient;
import org.jboss.resteasy.client.jaxrs.ResteasyClientBuilder;
import org.jboss.resteasy.client.jaxrs.ResteasyWebTarget;
import org.jboss.resteasy.plugins.providers.multipart.MultipartFormDataOutput;
import org.primefaces.model.file.UploadedFile;

import com.informaticapinguela.backing.Credenciais;
import com.informaticapinguela.backing.FileUpload;
/**
 * Clase para realizar consultas sobre los productos a la base de datos
 * @author Rubén Martínez Quiroga
 * @version 1.5
 */
@Stateless
public class ProductoDAO implements Serializable{
	private static final long serialVersionUID = 1L;
	private final String URL_BASE="http://localhost:8081/api/rest";
	ArrayList<Producto> productos= new ArrayList<Producto>();
	ArrayList<Categoria> categorias= new ArrayList<Categoria>();
	Producto producto;
	@PersistenceContext
	private EntityManager em;
	
	@Inject
	private Credenciais credenciais;

	
//	@Inject
//	private FileUpload file; 
	
	/**
	 * Método para listar todos los productos
	 * @return productos
	 */
	public ArrayList<Producto> getProductos() {
		String URL_PUNTO_SERVICIO = "/productos";
		ResteasyClient client = new ResteasyClientBuilder().build();
		ResteasyWebTarget target = client.target(URL_BASE+URL_PUNTO_SERVICIO);
		ArrayList<Producto> productos = 
				target.request().header("Authorization", "Bearer "+credenciais.getToken()).get(new GenericType<ArrayList<Producto>>() {});
		
		for(Producto producto: productos) {
			String URL_PUNTO_SERVICIO2="/productos/download/"+producto.getImage();
			ResteasyClient client2 = new ResteasyClientBuilder().build();
			ResteasyWebTarget target2 = client2.target(URL_BASE+URL_PUNTO_SERVICIO2);
			Response response = target2.request().header("Authorization", "Bearer "+credenciais.getToken()).get();
			InputStream in = response.readEntity(InputStream.class);
			Path path = Paths.get("C:\\jsf_images\\images\\", producto.getImage());
				try {
					Files.copy(in, path);
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			response.close();
		}
		
		return productos;
	}
	
	public void addRating(Producto p){
		String URL_PUNTO_SERVICIO = "/productos/"+p.getId();
		ResteasyClient client = new ResteasyClientBuilder().build();
		ResteasyWebTarget target = client.target(URL_BASE+URL_PUNTO_SERVICIO);
		Response response = target.request().header("Authorization", "Bearer "+credenciais.getToken()).put(Entity.entity(p, "application/json"));
		response.close();
	}
	
	/**
	 * Método para añadir un producto a la base de datos
	 * @param p nuevo
	 */
	public void addProducto(Producto p, UploadedFile file) {
		String URL_PUNTO_SERVICIO = "/productos";
		ResteasyClient client = new ResteasyClientBuilder().build();
		ResteasyWebTarget target = client.target(URL_BASE+URL_PUNTO_SERVICIO);
		Response response = target.request().header("Authorization", "Bearer "+credenciais.getToken()).post(Entity.entity(p, "application/json"));
		Producto productoConId = response.readEntity(Producto.class);
		System.out.println("addProducto: "+response.getStatus());
		response.close();
		
		// actualizar el propio fichero de imagen
		String fileName="";
      if (file != null) {
    	fileName = Paths.get(file.getFileName()).getFileName().toString(); 
    	System.out.println("ProductoDAO: filename: "+ fileName);
 	    String uploads = "C:\\jsf_images\\temp\\";
 	    //fileNameDest=fileName+"__"+UUID.randomUUID().toString()+fileName.substring(fileName.indexOf('.'));
 	    File fich = new File(uploads, fileName);
 	    try (InputStream input = file.getInputStream()) {
 	        Files.copy(input,fich.toPath());
// 	        FacesMessage message = new FacesMessage("Subida exitosa", event.getFile().getFileName() + " se ha subido.");
// 	        FacesContext.getCurrentInstance().addMessage(null, message);
 	    }
 	    catch (IOException e) {
// 	    	 FacesMessage message = new FacesMessage("Error", event.getFile().getFileName() + " no se ha subido.");
// 	    	 FacesContext.getCurrentInstance().addMessage(null, message);
 	    }	
		
 	   String URL_PUNTO_SERVICIO2 = "/productos/"+productoConId.getId()+"/upload";
		ResteasyClient client2 = new ResteasyClientBuilder().build();
		ResteasyWebTarget target2 = client2.target(URL_BASE+URL_PUNTO_SERVICIO2);
		System.out.println("ProductoDAO, addProducto, URL: "+URL_BASE+URL_PUNTO_SERVICIO2);
		MultipartFormDataOutput output = new MultipartFormDataOutput();
		output.addFormData("uploadedFile", fich, MediaType.APPLICATION_OCTET_STREAM_TYPE, fileName);
		Response response2 = target2.request().post(Entity.entity(output, MediaType.MULTIPART_FORM_DATA));
		response2.close();
		
      }
		
	}
	
	
	/**
	 * Método para listar los productos con categoría videojuego
	 * @return productos
	 */
	public ArrayList<Producto> mostrarVideojuegos(){
		productos= new ArrayList<Producto>();
		StoredProcedureQuery query = em.createNamedStoredProcedureQuery("MOSTRAR_CATEGORIAS_VIDEOJUEGOS");
		List<Producto> result = query.getResultList();
		for(int i=0; i<result.size(); i++) {
			Producto producto = (Producto)result.get(i);
			productos.add(producto);
		}
		return productos;
	}
	
	/**
	 * Método para listar los productos con categoría película
	 * @return productos
	 */
	public ArrayList<Producto> mostrarPeliculas(){
		productos= new ArrayList<Producto>();
		StoredProcedureQuery query = em.createNamedStoredProcedureQuery("MOSTRAR_CATEGORIAS_PELICULAS");
		List<Producto> result = query.getResultList();
		for(int i=0; i<result.size(); i++) {
			Producto producto = (Producto)result.get(i);
			productos.add(producto);
		}
		return productos;
	}
	
	/**
	 * Método para listar los productos con categoría software
	 * @return productos
	 */
	public ArrayList<Producto> mostrarSoftware(){
		productos= new ArrayList<Producto>();
		StoredProcedureQuery query = em.createNamedStoredProcedureQuery("MOSTRAR_CATEGORIAS_SOFTWARE");
		List<Producto> result = query.getResultList();
		for(int i=0; i<result.size(); i++) {
			Producto producto = (Producto)result.get(i);
			productos.add(producto);
		}
		return productos;
	}
	
	/**
	 * Método para editar un producto
	 * @param p nuevo
	 */
	public void actualizarProducto(Producto p, UploadedFile file) {
		//em.merge(p);
		// actualiza datos del producto. p.getImage() es el nombre del fichero de imagen
		String URL_PUNTO_SERVICIO = "/productos/"+p.getId();
		ResteasyClient client = new ResteasyClientBuilder().build();
		ResteasyWebTarget target = client.target(URL_BASE+URL_PUNTO_SERVICIO);
		Response response = target.request().header("Authorization", "Bearer "+credenciais.getToken()).put(Entity.entity(p, "application/json"));
		System.out.println("ProductoDAO, actualizarProducto: "+response.getStatus());
		response.close();
		
		// actualizar el propio fichero de imagen
		String fileName="";
      if (file != null && file.getFileName()!=null) {
    	fileName = Paths.get(file.getFileName()).getFileName().toString(); 
    	System.out.println("ProductoDAO: filename: "+ fileName);
 	    String uploads = "C:\\jsf_images\\temp\\";
 	    //fileNameDest=fileName+"__"+UUID.randomUUID().toString()+fileName.substring(fileName.indexOf('.'));
 	    File fich = new File(uploads, fileName);
 	    try (InputStream input = file.getInputStream()) {
 	        Files.copy(input,fich.toPath());
// 	        FacesMessage message = new FacesMessage("Subida exitosa", event.getFile().getFileName() + " se ha subido.");
// 	        FacesContext.getCurrentInstance().addMessage(null, message);
 	    }
 	    catch (IOException e) {
// 	    	 FacesMessage message = new FacesMessage("Error", event.getFile().getFileName() + " no se ha subido.");
// 	    	 FacesContext.getCurrentInstance().addMessage(null, message);
 	    }	
		
    	String URL_PUNTO_SERVICIO2 = "/productos/"+p.getId()+"/upload";
		System.out.println("actualizarProducto: "+URL_BASE+URL_PUNTO_SERVICIO2);
		ResteasyClient client2 = new ResteasyClientBuilder().build();
		ResteasyWebTarget target2 = client2.target(URL_BASE+URL_PUNTO_SERVICIO2);
		MultipartFormDataOutput output = new MultipartFormDataOutput();
		output.addFormData("uploadedFile", fich, MediaType.APPLICATION_OCTET_STREAM_TYPE, fileName);
		
		System.out.println("ProductoDAO, nombre fich: "+fich.getName());
		Response response2 = target2.request().post(Entity.entity(output, MediaType.MULTIPART_FORM_DATA));
		response2.close();
		
		
      }
	}
	
	/**
	 * Método para borar un producto
	 * @param p nuevo
	 */
	public void borrar(Producto p) {
		String URL_PUNTO_SERVICIO = "/productos/"+p.getId();
		ResteasyClient client = new ResteasyClientBuilder().build();
		ResteasyWebTarget target = client.target(URL_BASE+URL_PUNTO_SERVICIO);
		Response response = target.request().header("Authorization", "Bearer "+credenciais.getToken()).delete();	
		response.close();
	}
	
	
//	public void subirImagen(Producto p) {
//		String URL_PUNTO_SERVICIO = "/productos/"+p.getId()+"/upload";
//		ResteasyClient client = new ResteasyClientBuilder().build();
//		ResteasyWebTarget target = client.target(URL_BASE+URL_PUNTO_SERVICIO);
//		MultipartFormDataOutput output = new MultipartFormDataOutput();
//		output.addFormData("file", file.getFile(), MediaType.APPLICATION_OCTET_STREAM_TYPE);
//		Response response = target.request().post(Entity.entity(output, MediaType.MULTIPART_FORM_DATA));
//		response.close();
//	}
	

	/**
	 * Obtener un producto
	 * @return producto
	 */
	public Producto getProducto() {
		return producto;
	}

	/**
	 * Configurar un producto
	 * @param producto nuevo
	 */
	public void setProducto(Producto producto) {
		this.producto = producto;
	}

	/**
	 * Configurar una lista de productos
	 * @param productos nuevo
	 */
	public void setProductos(ArrayList<Producto> productos) {
		this.productos = productos;
	}

	/**
	 * Configurar uan lista de categorias
	 * @param categorias nuevo
	 */
	public void setCategorias(ArrayList<Categoria> categorias) {
		this.categorias = categorias;
	}
}
