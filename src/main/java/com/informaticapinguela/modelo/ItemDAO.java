package com.informaticapinguela.modelo;

import java.io.Serializable;
import java.util.ArrayList;

import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.ws.rs.client.Entity;
import javax.ws.rs.core.GenericType;
import javax.ws.rs.core.Response;

import org.jboss.resteasy.client.jaxrs.ResteasyClient;
import org.jboss.resteasy.client.jaxrs.ResteasyClientBuilder;
import org.jboss.resteasy.client.jaxrs.ResteasyWebTarget;

import com.informaticapinguela.backing.Credenciais;

@Stateless
public class ItemDAO implements Serializable{
	private static final long serialVersionUID = 1L;
	private final String URL_BASE="http://localhost:8081/api/rest";
	
	@PersistenceContext
	private EntityManager em;
	
	@Inject
	private Credenciais credenciais;
	
	public ArrayList<Item> getItemsCarrito(Carrito c) {
		String URL_PUNTO_SERVICIO = "/items/carrito/"+c.getId();
		ResteasyClient client = new ResteasyClientBuilder().build();
		ResteasyWebTarget target = client.target(URL_BASE+URL_PUNTO_SERVICIO);
		ArrayList<Item> items = 
				target.request().header("Authorization", "Bearer "+credenciais.getToken()).get(new GenericType<ArrayList<Item>>() {});
		return items;
	}
	
	
	public Item addItem(Item i) {
		String URL_PUNTO_SERVICIO = "/items/producto/"+i.getProducto().getId();
		ResteasyClient client = new ResteasyClientBuilder().build();
		ResteasyWebTarget target = client.target(URL_BASE+URL_PUNTO_SERVICIO);
		Response response = target.request().header("Authorization", "Bearer "+credenciais.getToken()).post(Entity.entity(i, "application/json"));
		Item item=response.readEntity(Item.class);
		System.out.println("ItemDAO, addItem: "+response.getStatus());
		response.close();
		return item;
	}
	
	
	public void asignaItemACarrito(Item i, Carrito c) {
		String URL_PUNTO_SERVICIO2 = "/items/"+i.getId()+"/carrito/"+c.getId();
		ResteasyClient client2 = new ResteasyClientBuilder().build();
		ResteasyWebTarget target2 = client2.target(URL_BASE+URL_PUNTO_SERVICIO2);
		Response response2 = target2.request().header("Authorization", "Bearer "+credenciais.getToken()).put(Entity.entity(i, "application/json"));
		System.out.println("ItemDAO, addItem: "+response2.getStatus());
		response2.close();
	}
	
	
	public void quitarItem(Item i) {
		String URL_PUNTO_SERVICIO = "/items/"+i.getId();
		System.out.println("ItemDAO, quitarItem: "+i);
		ResteasyClient client = new ResteasyClientBuilder().build();
		ResteasyWebTarget target = client.target(URL_BASE+URL_PUNTO_SERVICIO);
		Response response = target.request().header("Authorization", "Bearer "+credenciais.getToken()).delete();	
		response.close();
	}

}
