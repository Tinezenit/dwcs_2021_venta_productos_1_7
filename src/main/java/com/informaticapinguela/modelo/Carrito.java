package com.informaticapinguela.modelo;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.enterprise.context.Dependent;
import javax.inject.Named;
import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;

/**
 * Clase para obtener la información de los items
 * @author Rubén Martínez Quiroga
 * @version 1.5
 */
@Named
@Entity
@Dependent
public class Carrito implements Serializable{
	private static final long serialVersionUID = 1L;
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private long id;
//	@OneToMany(mappedBy="carrito", fetch=FetchType.EAGER, cascade=CascadeType.ALL)
//	private List<Item> items= new ArrayList<Item>();
	@ManyToOne
	private Usuario usuario;
	
	/**
	 * Obtener id
	 * @return id
	 */
	public long getId() {
		return id;
	}
	/**
	 * Introducir id
	 * @param id nuevo
	 */
	public void setId(long id) {
		this.id = id;
	}


	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + (int) (id ^ (id >>> 32));
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Carrito other = (Carrito) obj;
		if (id != other.id)
			return false;
		return true;
	}
	/**
	 * Obtener un usuario
	 * @return usuario
	 */
	public Usuario getUsuario() {
		return usuario;
	}
	/**
	 * Crear un usuario
	 * @param usuario nuevo
	 */
	public void setUsuario(Usuario usuario) {
		this.usuario = usuario;
	}
	@Override
	public String toString() {
		return "Carrito [id=" + id + ", usuario=" + usuario + "]";
	}

}

