package com.informaticapinguela.modelo;

import java.io.Serializable;
import java.math.BigDecimal;
import java.security.Principal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.enterprise.context.Dependent;
import javax.persistence.CascadeType;
import javax.persistence.CollectionTable;
import javax.persistence.Column;
import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.NamedStoredProcedureQuery;
import javax.persistence.OneToMany;
import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;

/**
 * Clase para obtener la información de los usuarios
 * @author Rubén Martínez Quiroga
 * @version 1.5
 */
@Dependent
@Entity
@NamedStoredProcedureQuery(
	    name = "MOSTRAR_USUARIOS", 
	    procedureName = "MOSTRAR_USUARIOS", 
	    resultClasses = {Usuario.class}
	)
@NamedQueries({
	@NamedQuery(name="Usuario.findAll", query="SELECT u FROM Usuario u"),
	@NamedQuery(name="Usuario.findByEmail", query="SELECT u FROM Usuario u WHERE u.email=:email"),
	@NamedQuery(name="Usuario.findByContrasena", query="SELECT u FROM Usuario u WHERE u.contrasena=:contrasena")
})

public class Usuario implements Serializable, Principal{
	private static final long serialVersionUID= 1L;
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private long id;
	private String name;
	private String contrasena;
	@Column(unique=true)
	private String email;
	private String direccion;
	private String telefono;
	private String image;
	
	@ElementCollection(targetClass=Rol.class, fetch=FetchType.EAGER)
	@Fetch (value = FetchMode.SUBSELECT)
	@Enumerated(EnumType.STRING)
	@CollectionTable(name="user_roles", joinColumns={@JoinColumn(name="user_id")})
	@Column(name="rol")
	private List<Rol> roles= new ArrayList<Rol>();

//	@OneToMany(mappedBy="usuario", fetch=FetchType.EAGER, cascade=CascadeType.ALL)
//	@Fetch (value = FetchMode.SUBSELECT)
//	private List<Carrito> carritos= new ArrayList<Carrito>();
	
	public Usuario() {
		super();
	}

	public Usuario(String contrasena, String email) {
		super();
		this.contrasena = contrasena;
		this.email = email;
	}
	
	/**
	 * Obtener una id
	 * @return id
	 */
	public long getId() {
		return id;
	}
	/**
	 * Configurar una id
	 * @param id nuevo
	 */
	public void setId(long id) {
		this.id = id;
	}
	/**
	 * Obtener un nombre
	 * @return nombre
	 */
	public String getName() {
		return name;
	}
	/**
	 * Configurar un nombre
	 * @param nombre nuevo
	 */
	public void setName(String nombre) {
		this.name = nombre;
	}
	/**
	 * Obtener una contrasena
	 * @return contrasena
	 */
	public String getContrasena() {
		return contrasena;
	}
	/**
	 * Configurar una contrasena
	 * @param contrasena nuevo
	 */
	public void setContrasena(String contrasena) {
		this.contrasena = contrasena;
	}
	/**
	 * Configurar un email
	 * @return email
	 */
	public String getEmail() {
		return email;
	}
	/**
	 * Configurar un email
	 * @param email nuevo
	 */
	public void setEmail(String email) {
		this.email = email;
	}
	/**
	 * Obtener una direccion
	 * @return direccion
	 */
	public String getDireccion() {
		return direccion;
	}
	/**
	 * Configurar una direccion
	 * @param direccion nuevo
	 */
	public void setDireccion(String direccion) {
		this.direccion = direccion;
	}
	/**
	 * Obtener un teléfono
	 * @return telefono
	 */
	public String getTelefono() {
		return telefono;
	}
	/**
	 * Configurar un teléfono
	 * @param telefono nuevo
	 */
	public void setTelefono(String telefono) {
		this.telefono = telefono;
	}
	/**
	 * Obtener una lista de roles
	 * @return roles
	 */
	public List<Rol> getRoles() {
		return roles;
	}
	/**
	 * Configurar una lista de roles
	 * @param roles nuevo
	 */
	public void setRoles(List<Rol> roles) {
		this.roles = roles;
	}
	/**
	 * Método para comprobar si un rol está en la lista de roles
	 * @param rol nuevo
	 * @return rol
	 */
	public boolean tieneRol(Rol rol) {
		return roles.contains(rol);
	}


	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((email == null) ? 0 : email.hashCode());
		result = prime * result + (int) (id ^ (id >>> 32));
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Usuario other = (Usuario) obj;
		if (email == null) {
			if (other.email != null)
				return false;
		} else if (!email.equals(other.email))
			return false;
		if (id != other.id)
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "Usuario [id=" + id + ", name=" + name + ", contrasena=" + contrasena + ", email=" + email
				+ ", direccion=" + direccion + ", telefono=" + telefono + ", roles=" + roles + "]";
	}

	public String getImage() {
		return image;
	}

	public void setImage(String image) {
		this.image = image;
	}


}
