package com.informaticapinguela.modelo;

public enum InventoryStatus {
    INSTOCK("En Stock"),
    OUTOFSTOCK("Fuera de Stock"),
    LOWSTOCK("Poco Stock");

    private String text;

    InventoryStatus(String text) {
        this.text = text;
    }

    public String getText() {
        return text;
    }
}
