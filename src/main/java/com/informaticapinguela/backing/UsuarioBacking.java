package com.informaticapinguela.backing;

import java.io.Serializable;
import java.util.ArrayList;
import javax.annotation.PostConstruct;
import javax.enterprise.context.SessionScoped;
import javax.faces.application.FacesMessage;
import javax.inject.Inject;
import javax.inject.Named;
import javax.servlet.http.Part;

import org.primefaces.PrimeFaces;

import com.informaticapinguela.modelo.ConsultaDAO;
import com.informaticapinguela.modelo.Usuario;
import com.informaticapinguela.modelo.UsuarioDAO;

@Named
@SessionScoped
public class UsuarioBacking implements Serializable{
	private static final long serialVersionUID = 1L;
	ArrayList<Usuario> usuarios= new ArrayList<Usuario>();
	@Inject 
	private ConsultaDAO consultaDAO;
	
	@Inject
	private UsuarioDAO usuarioDAO;
	
	@Inject
	private Usuario usuario;
	
	public void cargaDatos() {
		consultaDAO.cargarDatos();
	}
	
	@PostConstruct
	public void init() {
		this.usuarios=usuarioDAO.getUsuarios();
	}

	public ArrayList<Usuario> getUsuarios() {
		return usuarios;
	}

	public void setUsuarios(ArrayList<Usuario> usuarios) {
		this.usuarios = usuarios;
	}

	public UsuarioDAO getUsuarioDAO() {
		return usuarioDAO;
	}

	public void setUsuarioDAO(UsuarioDAO usuarioDAO) {
		this.usuarioDAO = usuarioDAO;
	}

	public Usuario getUsuario() {
		return usuario;
	}

	public void setUsuario(Usuario usuario) {
		this.usuario = usuario;
	}

	public void nuevoUsuario() {
		usuarioDAO.addUsuario(usuario);
		this.usuarios=usuarioDAO.getUsuarios();
		this.usuario=new Usuario();
	}
	
	public void guardarImagen(String imagen) {
		usuario.setImage(imagen);
		this.usuarios=usuarioDAO.getUsuarios();
		this.usuario=new Usuario();
	}
	
    public void mensajeCompra() {
        FacesMessage message = new FacesMessage(FacesMessage.SEVERITY_INFO, "Mensaje", "Compra realizada");

        PrimeFaces.current().dialog().showMessageDynamic(message);
    }
	
}
