package com.informaticapinguela.backing;

import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.ConverterException;
import javax.faces.convert.FacesConverter;

import com.informaticapinguela.modelo.Categoria;
/**
 * Clase converter para que el select muestre los datos
 * @author Rubén Martínez Quiroga
 * @version 1.5
 */
@FacesConverter(value="CategoriaConv", forClass=Categoria.class)
public class CategoriaConverter implements Converter{

	@Override
	public Object getAsObject(FacesContext context, UIComponent component, String value) throws ConverterException{
		if(value==null) {
			return null;
		}
		ProductoBacking datos= context.getApplication().evaluateExpressionGet(context, "#{productoBacking}", ProductoBacking.class);
		for(Categoria c:datos.getCategorias()) {
			if(c.getNombre().equals(value)) {
				return c;
			}
		}
		throw new ConverterException();
	}

	@Override
	public String getAsString(FacesContext context, UIComponent component, Object value) {
		if(value==null) {
			return null;
		}else {
			return value.toString();
		}
	}
}
