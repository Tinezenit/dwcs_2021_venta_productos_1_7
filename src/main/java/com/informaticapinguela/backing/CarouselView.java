package com.informaticapinguela.backing;

import java.io.Serializable;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;
import javax.enterprise.context.RequestScoped;
import javax.enterprise.context.SessionScoped;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import javax.inject.Named;

import org.primefaces.PrimeFaces;
import org.primefaces.event.SelectEvent;

import com.informaticapinguela.modelo.Carrito;
import com.informaticapinguela.modelo.CarritoDAO;
import com.informaticapinguela.modelo.InventoryStatus;
import com.informaticapinguela.modelo.Item;
import com.informaticapinguela.modelo.ItemDAO;
import com.informaticapinguela.modelo.Producto;
import com.informaticapinguela.modelo.ProductoDAO;

@Named
@SessionScoped
public class CarouselView implements Serializable {

    private static final long serialVersionUID = 1L;
    private Producto selectedProduct;
    private List<Producto> productos;
    private Carrito carrito;
    private Item itemSeleccionado;
    private List<Item> items;
    
    public void addMessage(FacesMessage.Severity severity, String summary, String detail) {
        FacesContext.getCurrentInstance().
                addMessage(null, new FacesMessage(severity, summary, detail));
    }
    
    @Inject
    private ProductoDAO productoDAO;
    
    @Inject
    private ItemDAO itemDAO;
    
    @Inject
    private CarritoDAO carritoDAO;
    
    @Inject
    private Credenciais credenciais;
    
    @PostConstruct
    public void init() {
    	productos = productoDAO.getProductos();
    	carrito= new Carrito();
    	carrito.setUsuario(credenciais.usuarioConectado());
    	Carrito carritoBD=carritoDAO.addCarrito(carrito);
    	carrito.setId(carritoBD.getId());
    	items=new ArrayList<Item>();
    	itemSeleccionado= new Item();
    	System.out.println("CarouselView, init:" +productos);
    }

    public void addItem(Producto p) {
    	itemSeleccionado.setProducto(p);
    	itemSeleccionado.setCarrito(carrito);
    	System.out.println("carouserView, addItem, itemSeleccionado: "+itemSeleccionado);
    	Item itemConId=itemDAO.addItem(itemSeleccionado);
    	items.add(itemConId);
    	itemDAO.asignaItemACarrito(itemConId, carrito);
    	System.out.println("CarouserView, addItem: "+itemConId+"A Carrito: "+carrito);
    	itemSeleccionado=new Item();
    	addMessage(FacesMessage.SEVERITY_INFO, itemConId.getProducto().getNombre()+" añadido", "");
    	
    }
    
    public void verCarrito()  {
    	 Map<String,Object> options = new HashMap<>();
         options.put("modal", true);
         options.put("width", 640);
         options.put("height", 340);
         options.put("contentWidth", "100%");
         options.put("contentHeight", "100%");
         options.put("headerElement", "customheader");

         PrimeFaces.current().dialog().openDynamic("carrito", options, null);
         
    }
    
    public void cerrarCarrito() {
   	 Map<String,Object> options = new HashMap<>();
     options.put("modal", true);
     options.put("width", 640);
     options.put("height", 340);
     options.put("contentWidth", "100%");
     options.put("contentHeight", "100%");
     options.put("headerElement", "customheader");

     PrimeFaces.current().dialog().closeDynamic("carrito");
    }
    
    public void showMessage() {
        FacesMessage message = new FacesMessage(FacesMessage.SEVERITY_INFO, "Message", " Always Bet on Prime!");

        PrimeFaces.current().dialog().showMessageDynamic(message);
    }
    
    public void quitarItem(Item i) {
    	itemDAO.quitarItem(i);
    	items.remove(i);
    }
    
    
    public List<Producto> getProductos() {
        return productos;
    }

    public void setProductos(List<Producto> productos) {
        this.productos = productos;
    }

    public Producto getSelectedProducto() {
        return selectedProduct;
    }

	public Carrito getCarrito() {
		return carrito;
	}

	public void setCarrito(Carrito carrito) {
		this.carrito = carrito;
	}

	public Item getItemSeleccionado() {
		return itemSeleccionado;
	}

	public void setItemSeleccionado(Item itemSeleccionado) {
		this.itemSeleccionado = itemSeleccionado;
	}

	public List<Item> getItems() {
		return items;
	}

	public void setItems(List<Item> items) {
		this.items = items;
	}


}

