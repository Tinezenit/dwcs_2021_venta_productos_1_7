package com.informaticapinguela.backing;

import java.io.Serializable;
import java.util.ArrayList;

import javax.annotation.PostConstruct;
import javax.enterprise.context.SessionScoped;
import javax.inject.Inject;
import javax.inject.Named;
import javax.ws.rs.client.Entity;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.jboss.resteasy.client.jaxrs.ResteasyClient;
import org.jboss.resteasy.client.jaxrs.ResteasyClientBuilder;
import org.jboss.resteasy.client.jaxrs.ResteasyWebTarget;
import org.jboss.resteasy.plugins.providers.multipart.MultipartFormDataOutput;
import org.primefaces.event.FileUploadEvent;
import org.primefaces.model.file.UploadedFile;

import com.informaticapinguela.modelo.Categoria;
import com.informaticapinguela.modelo.CategoriaDAO;
import com.informaticapinguela.modelo.Producto;
import com.informaticapinguela.modelo.ProductoDAO;

@Named
@SessionScoped
public class ProductoBacking implements Serializable{
	private static final long serialVersionUID = 1L;
	ArrayList<Producto> productos= new ArrayList<Producto>();
	ArrayList<Producto> productosVideojuegos= new ArrayList<Producto>();
	ArrayList<Producto> productosPeliculas= new ArrayList<Producto>();
	ArrayList<Producto> productosSoftware= new ArrayList<Producto>();
	ArrayList<Categoria> categorias= new ArrayList<Categoria>();
	
	private boolean nuevo=false;
	private boolean editar=false;
	private boolean videojuego=false;
	private boolean pelicula=false;
	private boolean software=false;
	
	
	@Inject
	private ProductoDAO productoDAO;
	
	@Inject 
	private CategoriaDAO categoriaDAO;
	
	@Inject
	private Producto producto;
	
	private UploadedFile file;
	
	public UploadedFile getFile() {
        return file;
    }

    public void setFile(UploadedFile file) {
        this.file = file;
    }

	@PostConstruct
	public void init() {
		this.productos=productoDAO.getProductos();
		this.categorias=categoriaDAO.getCategorias();
//		this.productosVideojuegos=productoDAO.mostrarVideojuegos();
//		this.productosPeliculas=productoDAO.mostrarPeliculas();
//		this.productosSoftware=productoDAO.mostrarSoftware();
	}
	
	 public void upload(FileUploadEvent event) {
		 
			final String URL_BASE="http://localhost:8081/api/rest";
	    	file=event.getFile();
	    	String URL_PUNTO_SERVICIO = "/productos/"+producto.getId()+"/upload";
    		System.out.println("FileUpload: "+URL_BASE+URL_PUNTO_SERVICIO);
    		ResteasyClient client = new ResteasyClientBuilder().build();
    		System.out.println("FileUpload, client: "+client.toString());
    		ResteasyWebTarget target = client.target(URL_BASE+URL_PUNTO_SERVICIO);
    		System.out.println("FileUpload, target: "+target.toString());
    		MultipartFormDataOutput output = new MultipartFormDataOutput();
    		System.out.println("FileUpload, output: "+output.toString());
    		output.addFormData("uploadedFile", file, MediaType.APPLICATION_OCTET_STREAM_TYPE);
    		System.out.println("FileUpload, output con file: "+output.toString());
    		Response response = target.request().post(Entity.entity(output, MediaType.MULTIPART_FORM_DATA));
    		response.close();
	    	
	    	
//	        if (file != null) {
//	        	String fileName = Paths.get(file.getFileName()).getFileName().toString(); 
//	     	    String uploads = "C:\\Ficheros\\productos\\";
//	     	    String fileNameDest="";
//	     	    System.out.println("upload: filename: "+fileName);
//	     	    System.out.println("upload: uploads : "+uploads);
//	     	    fileNameDest=fileName+"__"+UUID.randomUUID().toString()+fileName.substring(fileName.indexOf('.'));
//	     	    try (InputStream input = file.getInputStream()) {
//	     	        Files.copy(input, new File(uploads, fileNameDest).toPath());
//	     	        FacesMessage message = new FacesMessage("Subida exitosa", event.getFile().getFileName() + " se ha subido.");
//	     	        FacesContext.getCurrentInstance().addMessage(null, message);
//	     	    }
//	     	    catch (IOException e) {
//	     	    	 FacesMessage message = new FacesMessage("Error", event.getFile().getFileName() + " no se ha subido.");
//	     	    	 FacesContext.getCurrentInstance().addMessage(null, message);
//	     	    }	
//	        	
//	        
	    		
//	        }else {
//	        	System.out.println("upload: file is null");
//	        }
	    }
	
	public void nuevoProducto () {
		productoDAO.addProducto(producto, file);
		this.productos=productoDAO.getProductos();
		this.producto=new Producto();
		nuevo=false;
	}
	
	
	public void actualizar() {
		productoDAO.actualizarProducto(producto, file);
		this.productos=productoDAO.getProductos();
		this.producto= new Producto();
		editar=false;
	}
	

	public void editar(Producto p) {
		editar=true;
		producto=p;
	}
	
	public void delete (Producto p) {
		productoDAO.borrar(p);
		productos.remove(p);
	}
	
	public boolean isEditar() {
		return editar;
	}

	public void setEditar(boolean editar) {
		this.editar = editar;
	}
	
	public boolean isNuevo() {
		return nuevo;
	}

	public boolean nuevoV() {
		nuevo=true;
		return nuevo;
	}
	
	public ArrayList<Producto> getProductos() {
		return productos;
	}
	public void setProductos(ArrayList<Producto> productos) {
		this.productos = productos;
	}
	public ProductoDAO getProductoDAO() {
		return productoDAO;
	}
	public void setProductoDAO(ProductoDAO productoDAO) {
		this.productoDAO = productoDAO;
	}
	public Producto getProducto() {
		return producto;
	}
	public void setProducto(Producto producto) {
		this.producto = producto;
	}

	public void setNuevo(boolean nuevo) {
		this.nuevo = nuevo;
	}
	
	public ArrayList<Categoria> getCategorias() {
		return categorias;
	}

	public void setCategorias(ArrayList<Categoria> categorias) {
		this.categorias = categorias;
	}
	
	public void fVideojuego() {
		videojuego=true;
		pelicula=false;
		software=false;
	}
	
	public void fPelicula() {
		pelicula=true;
		videojuego=false;
		software=false;
	}
	
	public void fSoftware() {
		software=true;
		videojuego=false;
		pelicula=false;
	}
	
	public boolean isVideojuego() {
		return videojuego;
	}
	
	public boolean isPelicula() {
		return pelicula;
	}
	
	public boolean isSoftware() {
		return software;
	}
	
	public ArrayList<Producto> getProductosVideojuegos() {
		return productosVideojuegos;
	}

	public void setProductosVideojuegos(ArrayList<Producto> productosVideojuegos) {
		this.productosVideojuegos = productosVideojuegos;
	}

	public ArrayList<Producto> getProductosPeliculas() {
		return productosPeliculas;
	}

	public void setProductosPeliculas(ArrayList<Producto> productosPeliculas) {
		this.productosPeliculas = productosPeliculas;
	}

	public ArrayList<Producto> getProductosSoftware() {
		return productosSoftware;
	}

	public void setProductosSoftware(ArrayList<Producto> productosSoftware) {
		this.productosSoftware = productosSoftware;
	}
	
}
