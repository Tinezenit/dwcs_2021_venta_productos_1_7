package com.informaticapinguela.backing;

import java.io.IOException;
import java.io.Serializable;

import javax.enterprise.context.SessionScoped;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import javax.inject.Named;

import com.informaticapinguela.modelo.Rol;
import com.informaticapinguela.modelo.Usuario;
import com.informaticapinguela.modelo.UsuarioDAO;

/**
 * Clase credenciales para comprobar los roles del usuario logueado
 * @author Rubén Martínez Quiroga
 * @version 1.5
 */
@Named
@SessionScoped
public class Credenciais implements Serializable {
	private static final long serialVersionUID = 1L;
	private String email;
	private String password;
	private String rol;
	private boolean autenticado;
	private String token;

	@Inject
	UsuarioDAO usuarioDAO;
	
	/**
	 * Constructor que toma por defecto el rol_anon
	 */
	public Credenciais() {
		super();
		email = "";
		rol = "ROLE_ANON";
		autenticado = false;
	}
	/**
	 * Obtener un email
	 * @return email
	 */
	public String getEmail() {
		return email;
	}
	/**
	 * Configurar un email
	 * @param email nuevo
	 */
	public void setEmail(String email) {
		this.email = email;
	}
	/**
	 * Obtener un password
	 * @return password
	 */
	public String getPassword() {
		return password;
	}
	/**
	 * Configurar un password
	 * @param password nuevo
	 */
	public void setPassword(String password) {
		this.password = password;
	}
	/**
	 * Obtener un rol
	 * @return rol
	 */
	public String getRol() {
		return rol;
	}
	/**
	 * Configurar un rol
	 * @param rol nuevo
	 */
	public void setRol(String rol) {
		this.rol = rol;
	}
	/**
	 * Método para comprobar si está autenticado el usuario
	 * @return autenticado
	 */
	public boolean isAutenticado() {
		return autenticado;
	}
	/**
	 * Método que configura la comprobación si un usuario está autenticado
	 * @param autenticado nuevo
	 */
	public void setAutenticado(boolean autenticado) {
		this.autenticado = autenticado;
	}
	/**
	 * Método para sacar los datos de la clase
	 */
	@Override
	public String toString() {
		return "Credenciais [username=" + email + ", password=" + password + ", rol=" + rol + ", autenticado="
				+ autenticado + "]";
	}
	/**
	 * Método que conprueba que rol tiene el usuario y le asigna un destino según ese rol
	 * @return destino
	 */
	public String autentica() {
		Usuario usuarioEncontrado = null;
		String destino = "login";
		try {
			usuarioEncontrado = usuarioDAO.validaUsuario(email, password);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		if (usuarioEncontrado == null) {
			email = "";
			rol = "ROLE_ANON";
			autenticado = false;
		} else if (usuarioEncontrado.tieneRol(Rol.ROLE_ADMIN)) {
			System.out.println("zonaadmin");
			rol = "ROLE_ADMIN";
			autenticado = true;
			destino = "admin";
		} else if (usuarioEncontrado.tieneRol(Rol.ROLE_USER)) {
			System.out.println("zona users");
			rol = "ROLE_USER";
			autenticado = true;
			destino = "user";
		}
		return destino;
	}
	
	public Usuario usuarioConectado() {
		Usuario usuarioEncontrado = null;
		try {
			usuarioEncontrado = usuarioDAO.validaUsuario(email, password);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return usuarioEncontrado;
	}
	/**
	 * Método para desconectarte del usuario logueado
	 */
	public void logout() {
		email = "";
		rol = "ROLE_ANON";
		autenticado = false;
		System.out.println("Logout   =>  Rol:: ROLE_ANON");
		ExternalContext context = FacesContext.getCurrentInstance().getExternalContext();
		try {
			System.out.println(context.getRequestContextPath() + "/login.xhtml");
			context.redirect(context.getRequestContextPath() + "/login.xhtml");
		} catch (IOException e) {
			e.printStackTrace();
		}
		// return "index"; // falla
	}
	/**
	 * Método para configurar una redirección
	 */
	public void toUsuarios() {
		ExternalContext context = FacesContext.getCurrentInstance().getExternalContext();
		try {
			context.redirect(context.getRequestContextPath() + "/listarUsuarios.xhtml");
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	/**
	 * Método para configurar una redirección
	 */
	public void toProductos() {
		ExternalContext context = FacesContext.getCurrentInstance().getExternalContext();
		try {
			context.redirect(context.getRequestContextPath() + "/listarProductos.xhtml");
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	/**
	 * Método para configurar una redirección
	 */
	public void toAdmin() {
		ExternalContext context = FacesContext.getCurrentInstance().getExternalContext();
		try {
			context.redirect(context.getRequestContextPath() + "/admin.xhtml");
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public void toCarrito() {
		ExternalContext context = FacesContext.getCurrentInstance().getExternalContext();
		try {
			context.redirect(context.getRequestContextPath() + "/carrito.xhtml");
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	
	public void toTienda() {
		ExternalContext context = FacesContext.getCurrentInstance().getExternalContext();
		try {
			context.redirect(context.getRequestContextPath() + "/user.xhtml");
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	
	/**
	 * Método para configurar una redirección
	 */
	public void toRegistro() {
		ExternalContext context = FacesContext.getCurrentInstance().getExternalContext();
		try {
			context.redirect(context.getRequestContextPath() + "/registro.xhtml");
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	/**
	 * Método para configurar una redirección
	 */
	public void toLogin() {
		ExternalContext context = FacesContext.getCurrentInstance().getExternalContext();
		try {
			context.redirect(context.getRequestContextPath() + "/login.xhtml");
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	public String getToken() {
		return token;
	}
	public void setToken(String token) {
		this.token = token;
	}
}
