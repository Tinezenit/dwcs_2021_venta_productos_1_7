package com.informaticapinguela.backing;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.Serializable;

import javax.enterprise.context.RequestScoped;
import javax.enterprise.context.SessionScoped;
import javax.faces.application.FacesMessage;  
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import javax.inject.Named;
import javax.ws.rs.client.Entity;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.UUID;

import org.jboss.resteasy.client.jaxrs.ResteasyClient;
import org.jboss.resteasy.client.jaxrs.ResteasyClientBuilder;
import org.jboss.resteasy.client.jaxrs.ResteasyWebTarget;
import org.jboss.resteasy.plugins.providers.multipart.MultipartFormDataOutput;
import org.primefaces.event.FileUploadEvent;
import org.primefaces.model.file.UploadedFile;

import com.informaticapinguela.modelo.Producto;

@Named
@SessionScoped
public class FileUpload implements Serializable{
	private static final long serialVersionUID = 1L;
	private final String URL_BASE="http://localhost:8081/api/rest";
	private UploadedFile file;
	@Inject
	private Producto producto;
	

    public UploadedFile getFile() {
        return file;
    }

    public void setFile(UploadedFile file) {
        this.file = file;
    }

    public void upload(FileUploadEvent event) {
    	file=event.getFile();
//        if (file != null) {
//        	String fileName = Paths.get(file.getFileName()).getFileName().toString(); 
//     	    String uploads = "C:\\Ficheros\\productos\\";
//     	    String fileNameDest="";
//     	    System.out.println("upload: filename: "+fileName);
//     	    System.out.println("upload: uploads : "+uploads);
//     	    fileNameDest=fileName+"__"+UUID.randomUUID().toString()+fileName.substring(fileName.indexOf('.'));
//     	    try (InputStream input = file.getInputStream()) {
//     	        Files.copy(input, new File(uploads, fileNameDest).toPath());
//     	        FacesMessage message = new FacesMessage("Subida exitosa", event.getFile().getFileName() + " se ha subido.");
//     	        FacesContext.getCurrentInstance().addMessage(null, message);
//     	    }
//     	    catch (IOException e) {
//     	    	 FacesMessage message = new FacesMessage("Error", event.getFile().getFileName() + " no se ha subido.");
//     	    	 FacesContext.getCurrentInstance().addMessage(null, message);
//     	    }	
//        	
//        
    		String URL_PUNTO_SERVICIO = "/productos/"+producto.getId()+"/upload";
    		System.out.println("FileUpload: "+URL_BASE+URL_PUNTO_SERVICIO);
    		ResteasyClient client = new ResteasyClientBuilder().build();
    		System.out.println("FileUpload, client: "+client.toString());
    		ResteasyWebTarget target = client.target(URL_BASE+URL_PUNTO_SERVICIO);
    		System.out.println("FileUpload, target: "+target.toString());
    		MultipartFormDataOutput output = new MultipartFormDataOutput();
    		System.out.println("FileUpload, output: "+output.toString());
    		output.addFormData("uploadedFile", file, MediaType.APPLICATION_OCTET_STREAM_TYPE);
    		System.out.println("FileUpload, output con file: "+output.toString());
    		Response response = target.request().post(Entity.entity(output, MediaType.MULTIPART_FORM_DATA));
    		response.close();
//        }else {
//        	System.out.println("upload: file is null");
//        }
    }



}
