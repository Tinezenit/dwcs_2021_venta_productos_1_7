package com.informaticapinguela.backing;

import java.io.IOException;
import java.util.Hashtable;
import java.util.Map;

import javax.inject.Inject;
import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

@WebFilter("/tenda/*")
public class AuthFilter implements Filter {
	private HttpServletRequest httpRequest;
	private Hashtable<String, String[]> taboaAutorizacions;

	@Inject
	private Credenciais credenciais;

	public AuthFilter() {
	}
	public void destroy() {
	}
	public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain)
			throws IOException, ServletException {

		// para logging
		System.out.println("AuthFilter:: username: " + credenciais.getEmail() + "" + " rol: " + credenciais.getRol());
                //////// fin logging

		httpRequest = (HttpServletRequest) request;
		String path = httpRequest.getRequestURI().substring(httpRequest.getContextPath().length());

		// elimino jsessionid
		if (path.lastIndexOf("jsessionid") > 0) {
			path = path.substring(0, path.lastIndexOf("jsessionid") - 1);
			System.out.println("Path filtrado:: " + path);
		}

		// para logging
		System.out.println("Path:: " + path);
                ///////// fin logging

		if (credenciais.getEmail() == null) {
			credenciais.setEmail("");
			credenciais.setRol("ROLE_ANON");
			credenciais.setAutenticado(false);
		}
		boolean isAuthorised = isAuthorised(credenciais.getRol(), path);
		boolean isAnonymous = credenciais.getRol().equals("ROLE_ANON");

		// para logging
		if (isAuthorised) {
			System.out.println("User " + credenciais.getEmail() + " autorizado para: " + path);
		} else {
			System.out.println("User " + credenciais.getEmail() + " NO autorizado para: " + path);
		}
                //////////////////// fin logging

		if (!isAuthorised && isAnonymous) {
			// se non está autorizado e é o usuario anonymous, despachamos á vista de login
			String loginPage = "/login.xhtml";
			RequestDispatcher dispatcher = httpRequest.getRequestDispatcher(loginPage);
			dispatcher.forward(request, response);
		} else if (!isAuthorised) {
			// semnon é anonymous e non está autorizado despachamos ao controlador principal
			httpRequest.getRequestDispatcher("/").forward(request, response);

		} else {
			// se está autorizado, invocamos ao seguinte filtro na cadea
			chain.doFilter(request, response);
		}
//		/// PARA PROBAS 
	//	chain.doFilter(request, response);
	}

	public void init(FilterConfig fConfig) throws ServletException {
		// construo a taboa de autorizacións que conten para cada rol
		// unha lista das urls autorizadas
		taboaAutorizacions = new Hashtable<String, String[]>();
		String[] urlsADMIN = { "/favicon.ico", "/login.xhtml", "/index.xhtml", "/admin.xhtml", "/listarUsuarios.xhtml", "/listarProductos.xhtml", "/resources/css/style.css", "/resources/img/azul.jpg"};
		String[] urlsUSER = { "/favicon.ico", "/login.xhtml", "/index.xhtml", "/user.xhtml", "/resources/css/style.css", "/resources/img/azul.jpg" };
		String[] urlsANON = { "/favicon.ico", "/login.xhtml", "/index.xhtml", "/resources/css/style.css", "/resources/img/azul.jpg", "/registro.xhtml"};
		taboaAutorizacions.put("ROLE_USER", urlsUSER);
		taboaAutorizacions.put("ROLE_ADMIN", urlsADMIN);
		taboaAutorizacions.put("ROLE_ANON", urlsANON);
	}
	private boolean isAuthorised(String _rol, String _url) {
		// recorro o Hashtable de roles e listas de urls autorizadas comprobando
		// se as urls autorizadas para o _rol do parámetro incluen a
		// url á que se desexa acceder, enviada como parámetro _url
		for (Map.Entry<String, String[]> entrada : taboaAutorizacions.entrySet()) {
			String rol = entrada.getKey();
			String[] urls = entrada.getValue();
			if (rol.equals(_rol)) {
				for (String url : urls) {
					if (url.contains(_url)) {
						System.out.println(url + " contains " + _url);
						return true;
					}
				}
			}
		}
		return false;
	}
}